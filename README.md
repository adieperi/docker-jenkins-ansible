# docker-jenkins-ansible

Repository with the files to make a Docker image that contains Jenkins and Ansible

#### build image
```sh
$ docker build . --no-cache --tag jenkins-ansible
```


#### Run image
```sh
$ docker run -dt --name jensible -p 8080:8080 -p 50000:50000 -v volume:/var/jenkins_home jenkins-ansible:latest
```

